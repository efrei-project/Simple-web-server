from flask import Flask, render_template, request, redirect, url_for
import datetime

app = Flask(__name__)

@app.route('/begin')
def begin():
	return render_template('begin.html')

@app.route('/home', methods=['POST'])
def home():
	tmp = jsonify({
		'ip': request.remote_addr,
		'captain': request.form.get('captain')
		})
	with open("log.txt", "a") as my_file:
		my_file.write("at {} this response: {}\n".format('2',tmp))
	return render_template('home.html', message="Go find the one piece !")

@app.route('/result', methods=['POST'])
def result():
	tmp = request.form.get('code')
	if tmp == "codeRockEfrei2020":
		return render_template('success.html')
	if tmp == "Y29kZVJvY2tFZnJlaTIwMjA=":
		return redirect(url_for('home'))
	return render_template('failed.html')
	
@app.route('/submission', methods=['POST'])
def submit():
	tmp = request.form.get('flagteam')
	print(tmp)
	now = datetime.datetime.now()
	with open("log.txt", "a") as my_file:
		my_file.write("at {} this response: {}\n".format(now,tmp))
	return render_template('home.html', message="Luffy >>>> Kaido")
